package se.cygni.osd.springscopeserver;

import org.springframework.stereotype.Service;

@Service
public class DelayService {

    public void delay() throws InterruptedException {
	Thread.sleep(100);
    }

}
