package se.cygni.osd.springscopeserver;

public class Dto {

    private final int value;

    public Dto(String valueString) {
	value = Integer.parseInt(valueString);
    }

    public int getValue() {
	return value;
    }

}
