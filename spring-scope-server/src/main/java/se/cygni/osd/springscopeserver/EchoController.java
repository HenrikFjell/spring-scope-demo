package se.cygni.osd.springscopeserver;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
// @RequestScope
public class EchoController {

    @Autowired
    private DelayService delayService;

    private String value;

    @GetMapping("/echo/{input}")
    public Dto getEcho(@PathVariable("input") String input) throws InterruptedException {
	value = input;
	delayService.delay();
	return new Dto(value);
    }

}
