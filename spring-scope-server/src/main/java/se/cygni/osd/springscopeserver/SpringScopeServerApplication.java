package se.cygni.osd.springscopeserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringScopeServerApplication {

    public static void main(String[] args) {
	SpringApplication.run(SpringScopeServerApplication.class, args);
    }

}
