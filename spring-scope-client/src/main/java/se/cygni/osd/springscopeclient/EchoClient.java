package se.cygni.osd.springscopeclient;

import java.util.HashSet;
import java.util.Set;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;

/**
 * This class is intended for verification of the server's ability to handle
 * concurrent requests only. This class itself is not thread safe.
 *
 */
@Component
public class EchoClient {

    private static final int NUMBER_OF_REQUESTS = 100;

    @Autowired
    private Logger logger;

    private final Set<Integer> resultSet = new HashSet<>();
    private int resultCounter = 0;
    private WebClient webClient;

    public void makeRequests() {
	resultSet.clear();
	resultCounter = 0;
	logger.info("Will make requests");
	for (int value = 0; value < NUMBER_OF_REQUESTS; value++) {
	    makeRequest(value);
	}
	logger.info("Requests sent");
    }

    private void handleResult(Dto actual) {
	logger.info("Result is {}", actual.getValue());
	resultSet.add(actual.getValue());
	resultCounter++;
	if (resultCounter >= NUMBER_OF_REQUESTS) {
	    verifyResults();
	}
    }

    @PostConstruct
    private void init() {
	webClient = WebClient.create("http://localhost:8080");
    }

    private void makeRequest(int value) {
	webClient.get()
		.uri("echo/{value}", value)
		.retrieve()
		.bodyToMono(Dto.class)
		.doOnSuccess(this::handleResult)
		.doOnError(result -> logger.error("Request failed")).subscribe();
    }

    private void verifyResults() {
	if (resultSet.size() == NUMBER_OF_REQUESTS) {
	    logger.info("All requests successfull");
	} else {
	    logger.error("Only {} unique responses", resultSet.size());
	}
    }

}
