package se.cygni.osd.springscopeclient;

public class Dto {

    private int value;

    public int getValue() {
	return value;
    }

    public void setValue(int value) {
	this.value = value;
    }

}
